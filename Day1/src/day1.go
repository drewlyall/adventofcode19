package main

import (
	"os"
	"log"
    "bufio"
    "strconv"
)

func part1 (lines []string) {
    var fuel int
    for _, line := range lines {
        line, _ := strconv.Atoi(line)
        fuel += int ((line / 3) - 2)
    }
    log.Printf("Total: %v\n", fuel)
}

func part2 (lines []string) {
    var finalTotal int
    for _, line := range lines {
        line, _ := strconv.Atoi(line)
        modTotal := calculateFuel(line, 0)
        finalTotal += modTotal
    }
    log.Printf("Final Total: %v\n", finalTotal)
}

func calculateFuel(weight int, current int) (int) {
    fuel := (weight / 3)
    if (fuel < 2) {
        return current
    }
    fuel -= 2
    current += fuel
    return calculateFuel(fuel, current)
}

func main() {
	lines, err := readInputs("inputs")
    if err != nil {
        log.Fatalf("readInputs: %s", err)
    }
    //part1(lines);
    part2(lines);
}

func readInputs(path string) ([]string, error) {
    file, err := os.Open(path)
    if err != nil {
        return nil, err
    }
    defer file.Close()

    var lines []string
    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }
    return lines, scanner.Err()
}