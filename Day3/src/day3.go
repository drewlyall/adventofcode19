package main

import (
	"bufio"
	"log"
	"math"
	"os"
	"strconv"
	"strings"
)

func main() {
	paths, err := readInputs("inputs")
    if err != nil {
        log.Fatalf("readInputs: %s", err)
	}
	
	//part1(paths)
	part2(paths)
}

func part1(paths []string) {
	log.Printf("Manhattan: %v", findSmallestManhattan(pointIntersections(plotPointsBruteForce(paths[0]), plotPointsBruteForce(paths[1]))))
}

func part2(paths []string) {
	wire1 := plotPointsBruteForce(paths[0])
	wire2 := plotPointsBruteForce(paths[1])
	intersections := pointIntersections(wire1, wire2)
	
	var shortest int
	for _, coord := range intersections {
		timePath := (findIndex(wire1, coord) + 1) + (findIndex(wire2, coord) + 1)
		if (shortest == 0 || timePath < shortest) {
			shortest = timePath
		}
	}
	log.Printf("Shortest Time: %v", shortest)
}

func plotPointsBruteForce(path string) ([]string) {
	steps := strings.Split(string(path), ",")
	startx := 0
	starty := 0
	var allPoints []string

	
	for _, step := range steps {
		dist, _ := strconv.Atoi(step[1:len(step)])

		corner := 0
	    switch direction := string(step[0]); direction {
			case "L":
				for i := startx; i >= startx - dist; i-- {
					if corner != 0 {
						allPoints = append(allPoints, strconv.Itoa(i) + "," + strconv.Itoa(starty))
					}
					corner++
				}
				startx = startx - dist
			case "R":
				for i := startx; i <= startx + dist; i++ {
					if corner != 0 {
						allPoints = append(allPoints, strconv.Itoa(i) + "," + strconv.Itoa(starty))
					}
					corner++
				}
				startx = startx + dist
			case "U":
				for i := starty; i <= starty + dist; i++ {
					if corner != 0 {
						allPoints = append(allPoints, strconv.Itoa(startx) + "," + strconv.Itoa(i))
					}
					corner++
				}
				starty = starty + dist
			case "D":
				for i := starty; i >= starty - dist; i-- {
					if corner != 0 {
						allPoints = append(allPoints, strconv.Itoa(startx) + "," + strconv.Itoa(i))
					}
					corner++
				}
				starty = starty - dist
		}
	}
	return allPoints
}

func pointIntersections(points1, points2 []string) (c []string) {
	m := make(map[string]bool)

	for _, point := range points1 {
		m[point] = true
	}

	for _, point := range points2 {
		if _, ok := m[point]; ok {
			if point != "0,0" {
				c = append(c, point)
			}
		}
	}
	return
}

func findIndex(points []string, value string) int {
    for p, v := range points {
        if (v == value) {
            return p
        }
    }
    return -1
}

func findSmallestManhattan(intersections []string) (smallest int) {
	for _, current := range intersections {
		if current == "0,0" {
			continue
		}
		str := strings.Split(current, ",")
		xcoord, _ := strconv.Atoi(str[0])
		ycoord, _ := strconv.Atoi(str[1])
		manhattanDist := int(math.Abs(float64(xcoord))) + int(math.Abs(float64(ycoord)))
		if smallest == 0 {
			smallest = manhattanDist
		} else if manhattanDist < smallest {
			smallest = manhattanDist
		}
	}
	return
}

func readInputs(path string) ([]string, error) {
    file, err := os.Open(path)
    if err != nil {
        return nil, err
    }
    defer file.Close()

    var paths []string
    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
        paths = append(paths, scanner.Text())
    }
    return paths, scanner.Err()
}