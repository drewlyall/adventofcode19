package main

import (
	"log"
	"io/ioutil"
	"strings"
	"strconv"
)

var haltCode = 99
var addCode = 1
var multiCode = 2
var tick = 4

var output = 19690720

func processIntCode(program []int) (int) {
	loc := 0
	opcode := 0
	programLength := len(program)

	for (opcode != haltCode && loc <= (programLength-tick)) {

		opcode = program[loc]
		var address int
		address = program[loc+1]
		param1 := program[address]
		address = program[loc+2]
		param2 := program[address]
		writeLoc := program[loc+3]

		if(opcode == addCode) {
			program[writeLoc] = param1 + param2
		} else if (opcode == multiCode) {
			program[writeLoc] = param1 * param2
		}
		loc += tick;
	}
	return program[0];
}

func part1() {
	programCode := readInput("input")
	programCode[1] = 12
	programCode[2] = 2
	log.Printf("Location 0: %v", processIntCode(programCode))
}

func part2() {
	found := false
	for i := 0; i < 100; i++ {
		for j := 0; j < 100; j++ {
			programCode := readInput("input")
			programCode[1] = i
			programCode[2] = j

			processOutput := processIntCode(programCode)
			if processOutput == output {
				found = true
				log.Printf("Final Noun: %v, Verb: %v", i, j)
			}
			if found {
				break
			}
		}
		if found {
			break
		}
	}
}

func main() {
	//part1()
	part2()
}

func readInput(path string) ([]int){
	b, err := ioutil.ReadFile(path) // just pass the file name
    if err != nil {
        log.Fatalf("readInput: %s", err)
	}
	
	str := strings.Split(string(b), ",")
	var intcode = []int{}
	for _, i := range str {
		current, _ := strconv.Atoi(i)
		intcode = append(intcode, current)
	}
	return intcode
}